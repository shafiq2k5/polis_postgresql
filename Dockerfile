FROM centos:7
MAINTAINER uzzal, uzzal2k5@gmail.com
WORKDIR /postSql
COPY postsql.sh .
RUN chmod a+x postsql.sh
RUN sh postsql.sh
RUN curl -L -O https://github.com/pol-is/polisServer/tree/master/postgres/db_setup_draft.sql
#RUN psql -d polis -f db_setup_draft.sql

